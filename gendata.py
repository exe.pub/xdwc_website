import random
from xon_db import XonoticDB

first_place_means = [20., 30., 25., 44., 96.]
players = {
    'donald': 0,
    'op3': 0.01,
    'dekry': 0.02,
    'fetus': 0.03,
    'dizzy': 0.03,
    'barney': 0.04,
    'curse starwind': 0.05,
    'dante': 0.05,
    'matuka': 0.06,
    'homs': 0.07,
    'mirio': 0.09,
    'sinety': 0.1,
    'z': 0.13,
    'kasebrot': 0.15,
    'morosophos': 0.16,
    'kentone': 0.22,
    'packer': 0.44,
    'vova-jumper': 0.98
}


def get_map_name(ix):
    return 'xdwc2018-0%d' % (ix + 1)


def genresults(ix):
    first_place_mean = first_place_means[ix]
    results = []
    for name, relative in players.items():
        if ix % 2 == 0:
            relative *= 2
        mean = first_place_mean + first_place_mean * relative
        sigma = mean / 60
        time = random.gauss(mean, sigma)
        results.append((name, time))
    return sorted(results, key=lambda x: x[1])


def genspeed():
    player = random.choice(list(players.keys())[:8])
    speed = random.gauss(2000, 300)
    return player, speed


db = XonoticDB('8192\n')

for i, _ in enumerate(first_place_means):
    map_name = get_map_name(i)
    p, s = genspeed()

    db[f'{map_name}/cts100record/speed/crypto_idfp'] = p
    db[f'{map_name}/cts100record/speed/speed'] = str(s)
    pos = 1
    for k, v in genresults(i):
        pos += 1
        db[f'{map_name}/cts100record/crypto_idfp{pos}'] = k
        db[f'{map_name}/cts100record/time{pos}'] = str(int(v * 100))
        db[f'/uid2name/{k}'] = k
db.save('test.db')

