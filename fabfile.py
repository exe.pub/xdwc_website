from fabric.api import *


env.hosts = ['xon.teichisma.info']
env.user = 'root'
local_user = 'xdwc'
local_home = '/home/xdwc'
app_path = '/home/xdwc/xdwc_website'


def update_app():
    with cd(app_path):
        sudo('git pull', user=local_user)


def install_requirements():
    with cd(app_path):
        sudo('HOME=%s pipenv install' % (local_home, ), user=local_user)


def update_systemd():
    put('conf/xdwc.service', '/etc/systemd/system/')
    run('systemctl daemon-reload')


def update_nginx():
    put('conf/nginx-site.conf', '/etc/nginx/sites-enabled/80xdwc.teichisma.info.conf')
    run('systemctl restart nginx')


def restart_server():
    run('systemctl restart xdwc')


def update_cron():
    put('conf/crontab', '/etc/cron.d/xdwc')
    run('chmod 644 /etc/cron.d/xdwc')


def migrate():
    with cd(app_path):
        sudo('HOME=%s pipenv run python ./manage.py migrate' % (local_home, ), user=local_user)


def scan_countries():
    with cd(app_path):
        sudo('HOME=%s pipenv run python ./manage.py scan_countries' % (local_home,), user=local_user)


def deploy():
    update_app()
    install_requirements()
    migrate()
    restart_server()
