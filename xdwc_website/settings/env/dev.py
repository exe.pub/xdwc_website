import os

DEBUG = True
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'ye0)*%((=l3*n=##4qc#7)ihy2y^&8yqq2-_-osq-^07u#s@yl'

INTERNAL_IPS = ['127.0.0.1', '::1']
ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'xdwc',
    }
}
